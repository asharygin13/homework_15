

#include <iostream>

void Numbers(int N, bool X)
{
    for (int a = 0; a <= N; ++a)
    {
        // ���� ����� true, ������� �������� �����
        // ���� ����� false, ������� ������ ����� 
        if (X == (a & 1))
        {
            std::cout << a << " ";
        }
    }
    std::cout << std::endl;
}

int main()
{
    const int N = 50;
    
    // ����� ������ ����� �� 0 �� 50
    std::cout << "0 - " << N << ": ";
    Numbers(N, false);

    // ����� �������� ����� �� 0 �� 50
    std::cout << "0 - " << N << ": ";
    Numbers(N, true);

    return 0;
}

